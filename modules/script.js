let root;
let list;
const fs            = require('fs');
const { promisify } = require('util');
const readFile      = promisify( fs.readFile );
const writeFile     = promisify( fs.writeFile );
const stat          = promisify( fs.stat );
const { print }     = require('../modules/print_methods');
const require_tree  = require('require-tree');
const methods       = require_tree('./script_methods');
const {splitCommand}= require('./commander_async');

const Operator = class {

	constructor(){
		this.known_children = {};
		this.children = [];
		this.params = {};
		this.result; // Array of strings
	}

	init( parent, commandline){
		this.parent = parent;
		this.commandline = splitCommand( commandline );
	}

	help(){
		return this.description + '\n' + 'Синтаксис:\n\n' + this.syntax + '\nДочерние элементы:\n\n' + Object.keys(this.known_children).join(', ') + '\n';
	}

	accept( i, line, delemiter ){
		let opers = Object.keys(this.known_children).map( oper =>{
			return {re: RegExp( '^' + oper + '(.*)$'), class: this.known_children[oper] };
		})
		let lst;
		let flag = false;
		let m;
		for(let oper of opers){
			m = line.match(oper.re);
			if( m ){
				let c = oper.class;
				lst = new c();
				this.children.push( lst );
				lst.init( this, ( m[1] || '' ).replace(/^ +/, '') );
				flag = true;
				break;
			}
		}
		if( !flag ){
			m = line.match( RegExp( delemiter + '(.*)$') );
			if( m && (this.children.length>0) ){
				lst = this.children[ this.children.length-1 ];
				lst.accept( i, m[1], delemiter );
			}else{
				throw new Error( 'Неизвестная команда: "' + line + '" в строке #' + ( 1 + 1*i ).toString() + '. Контекст оператора "' + this.description + '"' );
			};
		}
	}

	set(param, value){
		this.params[param] = value;
	}

	get( param ){
		return this.params[ param ];
	}

	async go( datain ){
		this.datain = datain;
		if( typeof( this.befor ) == 'function' ) await this.befor();
		for( let child of this.children ){
			if( typeof( child.go ) == 'function' ){
				this.result = await child.go( this.result );
				if( typeof( this.after_each ) == 'function' ) await this.after_each();
			}
		}
		if( typeof( this.after ) == 'function' ){
			return await this.after();
		}else{
			return this.result;
		}
	}

}

const Iterate = class extends Operator {
	constructor(){
		super();
		this.text = [ ];
		delete this.known_children;
		this.description = "Перебор строк базы хостов";
		this.syntax = "!!iterate [<filter-expression>] [<param>]";
	}

	accept(i, line, delemiter ){
		this.text.push( line );
	}

	after(){
		let total = [];
		Object.keys(list).map( mac=>{
			return Object.assign({mac}, list[mac]);
		})
		.filter( ({ ip='', mac='', hostname='', net='', desc='', desc2='', ext='' })=>{
			return eval('(' + this.commandline[0] + ')');
		})
		.forEach( ( rec, i ) => {
			for( let l of this.text ){
				let l1 = l;
				for( let k in rec ){
					let rg = '\\$\\{' + k +'\\}'
					let regexp = RegExp(rg, 'g');
					l = l.replace( regexp, rec[k] );
					rg = '\\$\\{i\\}'
					regexp = RegExp(rg, 'g');
					l = l.replace( regexp, i );
				}
				// если в строке нет выражений подходящих под третий параметр то убираем макроподстановки, которыех ещё не было
				if( !this.commandline[1] || !l.match( RegExp(this.commandline[1]) ) ) l = l.replace( /\$\{.+?\}/g, '');
				total.push( l );
			}
		});
		return total;
	}
}

const Replace = class extends Operator {
	constructor(){
		super();
		delete this.known_children
		this.description = "Замена";
		this.syntax = "!!replace <filter-expression> <method>";
	}
	async after(){
		let total = this.datain.map( l => {
			let m;
			let re = RegExp( this.commandline[0] );
			m = l.match( re );
			if(m){
				let str = this.commandline[1]?(methods[this.commandline[1]])(m[0]):'';
				l = l.replace( re, str );
			}
			return l;
		})
		return total;
	}
}

const Morph = class extends Operator {
	constructor(){
		super();
		this.known_children['!!var'] = Var;
		this.known_children['!!lines'] = Lines;
		this.known_children['!!template'] = Template;
		this.description = "Изменение участка конфига";
		this.syntax = "!!morph <block-start-regexp> <block-end-regexp>";
		this.templ;
	}
	async go(datain){
		this.datain = datain;
		this.block = null;
		this.records = [];
		this.i = 0;
		this.block = this.getnext();
		while(this.block){
			for( let child of this.children ){
				if( child.syntax.match(/\!\!templ/) ){
					this.templ = child;
				}else{
					if( typeof( child.go ) == 'function' ){
						this.result = this.block;
						this.result = await child.go( this.result );
						//if( typeof( this.after_each ) == 'function' ) await this.after_each();
					}
				};
			}
			this.records.push( this.params );
			this.params = [];
			this.block = this.getnext();
		}
		if( !this.templ ) throw new Error('Для элемента !!morph необходимо определить шаблона в дочернем елементе !!template');
		this.templ.go([]);
		let total = [];
		for( let rec of this.records ){
			for( let l of this.params['text']){
				for( let k in rec ){
					l = l.replace(RegExp('\\$\\{' + k + '\\}', 'g'), rec[k])
				}
				let m = l.match(/^\!\!if +(.+?)\!\!(.*)$/);
				if( m ){
					let r = eval( '(' + m[1] + ')');
					if(r) total.push( m[2] );
				}else total.push( l );
			}
		}
		return total;
	}

	getnext(){
		let re1 = RegExp( this.commandline[0] );
		let re2 = this.commandline[1]?RegExp( this.commandline[1] ):null;
		let start;
		while( this.i < this.datain.length ){
			if( this.datain[this.i].match(re1) ){
				start = this.i;
				this.i++;
				if(this.i >= this.datain.length) break;
			}else{
				this.i++;
				if(this.i >= this.datain.length) break;
			}
			if( re2 ){
				if( this.datain[ this.i ].match( re2 ) ){
					if( typeof(start)!='undefined' ){
						this.i++;
						break;
					}
				}
				this.i++;
			}else{
				if( typeof(start)!='undefined' ){
					break;
				}
			}
		}
		if( typeof(start)!='undefined' ){
			return this.datain.slice( start, this.i );
		}else{
			return null;
		}
	}
}

// амигренин


const Var = class extends Operator {
	constructor(){
		super();
		delete this.known_children
		this.description = "Определение переменной";
		this.syntax = "!!var <varname> <regexp>";
	}
	async after(){
		let re = RegExp(this.commandline[1]);
		for( let l of this.datain){
			let m = l.match(re);
			if( m ){
				this.parent.set(this.commandline[0], m[1].replace(/^ +/,'').replace(/ +$/,'') );
			}
		}
	}
}

const Template = class extends Operator {

	constructor(){
		super();
		delete this.known_children
		this.description = "Определение шаблона вывода";
		this.syntax = "!!template";
		this.text = [];
	}

	accept( i, line, delemiter ){
		this.text.push( line );
	}

	async after(){
		this.parent.set('text', this.text);
	}
}

const Print = class extends Operator {
	constructor(){
		super();
		delete this.known_children
		this.description = "Простой вывод";
		this.syntax = "!!print";
		this.text = [];
	}

	accept(i, line, delemiter ){
		this.text.push( line );
	}

	async after(){
		return this.text;
	}
}


const Filter = class extends Operator {
	constructor(){
		super();
		delete this.known_children;
		this.description = "Фильтр";
		this.syntax = "!!filter [emptystring|doubleempystring]";
	}
	async after(){
		if( this.commandline[0] == 'emptystring' ){
			let total = [];
			if( this.commandline[1] ){
				let re  = RegExp( this.commandline[1] );
				let flag = false;
				for( let l of this.datain ){
					let isempty = !l;
					if( isempty ){
						if( !flag ) total.push( l );
					}else{
						total.push( l );
						flag = !!l.match( re );
					}
				}
				return total;
			}else{
				return this.datain.filter( l => { return l });
			}
		}else if (this.commandline[0] == 'notmatch'){
			let total = [];
			let re  = RegExp( this.commandline[1] );
			for( let l of this.datain ){
				if( !l.match(re) ) total.push( l );
			}
			return total;
		}else if (this.commandline[0] == 'doubleempystring'){
			let total = [];
			let flag = false;
			for( let l of this.datain ){
				let isempty = !!!l;
				if( isempty ){
					if( !flag ) total.push( l );
				}else{
					total.push( l );
				}
				flag = isempty;
			}
			return total;
		}else{
			return [].concat( this.datain );
		}
	}
}

const Block = class extends Operator {
	constructor(){
		super();
		this.known_children['!!iterate'] = Iterate;
		this.known_children['!!replace'] = Replace;
		this.known_children['!!filter'] = Filter;
		this.known_children['!!morph'] = Morph;
		this.known_children['!!print'] = Print;
		this.description = "Блок строк по меткам";
		this.syntax = "!!block [<start-mark>] [<end-mark>]";
	}
	async befor(){
		this.blockstart;
		this.blockend;

		for( let i in this.datain ){
			let line = this.datain[ i ];
			if( this.commandline[0] && line.match(RegExp(this.commandline[0])) ) this.blockstart = parseInt( i )  + 1;
			if( this.commandline[1] && line.match(RegExp(this.commandline[1])) ) this.blockend = parseInt( i );
		}

		if( this.commandline[0] && typeof(this.blockstart)!='number' ) throw new Error('Стартовая метка не найдена');

		if( typeof(this.blockstart)=='number' ){
			if( typeof(this.blockend)=='number' ){
				this.result = this.datain.slice( this.blockstart, this.blockend );
			}else{
				this.result = this.datain.slice( this.blockstart );
			}
		}else{
			this.result = [].concat( this.datain );
		}
	}

	async after(){
		let total;
		if( typeof(this.blockstart)=='number' ){
			total = this.datain.slice( 0, this.blockstart );
		}else{
			total = [];
			//.concat( this.argv.lines );
		}
		total = total.concat( this.result );
		if( typeof(this.blockend)=='number' ){
			total = total.concat( this.datain.slice( this.blockend ) );
		}
		return total;
	}

}

const Lines = class extends Operator {
	constructor(){
		super();
		this.known_children['!!replace'] = Replace;
		this.known_children['!!iterate'] = Iterate;
		this.known_children['!!filter'] = Filter;
		this.known_children['!!var'] = Var;
		this.known_children['!!print'] = Print;
		this.description = "Блок строк по номерам";
		this.syntax = "!!block [<shift-of-first-line>] [<lines-number>]";
	}
	async befor(){
		this.shift = parseInt( this.commandline[0] );
		if( typeof(this.commandline[1])!='undefined' ){
			this.limit = parseInt( this.commandline[1] );
		}else{
			this.limit = this.datain.length - this.shift;
		}
		if( this.shift<0 ){
			this.result = []
		}else{
			this.result = this.datain.slice( this.shift, this.shift + this.limit );
		}
	}
	async after(){
		let total;
		if( this.shift<0 ){
			total = [].concat( this.datain );
			total = total.concat( this.result );
		}else{
			total = this.datain.slice( 0, this.shift );
			total = total.concat( this.result );
			total = total.concat( this.datain.slice( this.shift + this.limit ) );
		}
		return total;
	}

	set( param, value ){
		this.parent.set(param, value);
		//this.params[param] = value;
	}

}

const Src = class extends Operator {
	constructor(){
		super();
		this.known_children['!!block'] = Block;
		this.known_children['!!lines'] = Lines;
		this.description = "Обработчик файла";
		this.syntax = "!!src <path-to-file>";
	}
	async befor(){
		let text = await readFile( this.commandline[0], 'UTF8' );
		this.result = text.split(/\r{0,1}\n/)
	}
	async after(){
		let outputfile = this.commandline[1] || this.commandline[0];
		if( outputfile == '-' ){
			process.stdout.write( this.result.join('\n') );
		}else{
			await writeFile( outputfile, this.result.join('\n'), 'utf8' );
		}
	}
}

const Description = class extends Operator {
	constructor(){
		super();
		delete this.known_children;
		this.description = "Имя скрипта";
	}
	init( parent, commandline){
		this.parent = parent;
		this.commandline = splitCommand( commandline ) ;
		this.parent.set( 'description', this.commandline[0] );
	}
}


const Root = class extends Operator {
	constructor(){
		super();
		this.known_children['!!src'] = Src;
		this.known_children['!!description'] = Description;
		this.description = "Корневой элемент";
	}
}

exports.load = function( lines, delemiter ){
	root = new Root();
	for( let i in lines ){
		// волочаевска 164
		if( !/^#/.test(lines[i]) ) root.accept( i, lines[i], delemiter );
	}
}

exports.description = async function(){
	return root.get('description');
}

exports.go = async function( db ){
	list = db.list;
	try{
		await root.go([]);
	}catch( err ){
		print( err.message );
	}
}
