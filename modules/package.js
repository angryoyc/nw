let pkg;
let needsave = false;
let  pathtopackagejson;

const ESC           = '\x1b[';
const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const stat = promisify(fs.stat);
const { print }            = require('../modules/print_methods');

exports.init = function( path ){

	pathtopackagejson = path;

	pkg = require( pathtopackagejson );
	if(!pkg.config){
		pkg.config = {};
		needsave = true;
	}

	if(!pkg.config.dbpath){
		pkg.config.dbpath = '/var/lib/' + (pkg.name || 'myname') + '/db.json';
		needsave = true;
	}

	return pkg;
}


exports.get = function(){
	return pkg;
}

exports.needsave = function(){
	return needsave;
}

exports.a_save = async function(opt){
	await writeFile( pathtopackagejson, JSON.stringify(pkg, null, 4), 'utf8');
	info('Файл пакета ' + pathtopackagejson + ' изменён! ', opt );
	needsave = false;
}


function info(mess, {nocolor=false, mute=false}){
	if( !mute ) print( (!nocolor?ESC + '2m':'') + mess + (!nocolor?ESC + '0m':''));
}