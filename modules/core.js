let pkg;
let pathtodbjson;
let db;

const ESC             = '\x1b[';
const fs              = require('fs');
const { promisify }   = require('util');
const writeFile       = promisify(fs.writeFile);
const readFile        = promisify(fs.readFile);
const stat            = promisify( fs.stat );
const readdir         = promisify( fs.readdir );
const chown           = promisify(fs.chown);
const chmod           = promisify(fs.chmod);
const { print }       = require('../modules/print_methods');
const { print_table } = require('../modules/print_methods');
const newdb           = '({})';
const spawn           = require('child_process').spawn;
const parser          = require('./script');


exports.a_init = async function( p ){
	pkg = pkg || p;
	pathtodbjson = pkg.get().config.dbpath;
	if( pkg.needsave() ) await pkg.a_save({});
	await createNewDbFileIfNotExist({});
	db = require(pathtodbjson);
}

exports.get = function(){
	return pkg;
}

exports.db = function(){
	return db;
}

exports.a_save = async function( opt ){
	await writeFile(pathtodbjson, JSON.stringify(db, null, 4), 'utf8');
	info('Сохранено!', opt);
}

exports.setDBpath = async function( newpath, opt ){
	pathtodbjson = pkg.get().config.dbpath = newpath;
	await createNewDbFileIfNotExist(opt);
	await pkg.a_save( opt );
	db = require(pathtodbjson);
}


exports.print_host = function( host, opt ){
	print_table(
		[
			{column:'par', width: 17, title:'Параметр'},
			{column:'val', width: 40, title:'Значение'},
		],
		Object.keys(host).map( par => {
			return {par, val:host[par]};
		})
	)
}

exports.set = async function( {mac='', param='', value='', nocolor=false, mute=false, notsave=false } ){
	if( mac ){
		let host = exports.mac( mac );
		if( param ){
			if( value ){
				if(  param=='hostname' ){
					letmacofexisten = param_exist( value, 'hostname' );
					if( letmacofexisten && letmacofexisten!=mac ) throw new Error( 'Имя хоста ' + value + ' уже используется' ) ;
				} 

				if(  param=='ip' ){
					letmacofexisten = param_exist( value, 'ip' );
					if( letmacofexisten && letmacofexisten!=mac ) throw new Error( 'IP ' + value + ' уже используется' ) ;
				} 

				host[ param ] = value;
				info( 'Новое значение ' + param + '=' + value, {nocolor, mute});
				if( !notsave ) await exports.a_save({ nocolor, mute });
			}else{
				if( param in host ){
					print(host[ param ]);
				}else{
					throw new Error('Параметр отсутствует');
				}
			}
		}else{
			exports.print_host( host );
		}
	}else{
		throw new Error('Необходимо указать MAC-адрес в виде: e4:8d:8c:0d:0d:7d');
	}
}


exports.mac = function( mac ){
	if( mac.length != 17 ) throw new Error('Неверный формат MAC-адреса');
	if( mac in db.list){
		return db.list[ mac ];
	}else{
		throw new Error('Неизвестный MAC-адрес');
	}
}

exports.del = async function( mac, opt ){
	if( mac.length != 17 ) throw new Error('Неверный формат MAC-адреса');
	if(!db.list) db.list = {};
	if( mac in db.list ){
		if( 'param' in opt){
			delete db.list[ mac ][ opt['param'] ];
		}else{
			delete db.list[ mac ];
		}
		if( !opt.notsave ) await exports.a_save( opt );
	}else{
		throw new Error('MAC-адрес не найден');
	}
}

exports.add = async function( mac, hostname, opt ){
	if( mac.length != 17 ) throw new Error('Неверный формат MAC-адреса');
	if(!db.list) db.list = {};
	if( mac in db.list ){
		if( db.list[ mac ].hostname!=hostname){
			info('Данный MAC-адрес уже присутствует. Имя будет изменено!', opt);
			db.list[ mac ].hostname = hostname;
			if( !opt.notsave ) await exports.a_save( opt );
		}
		return db.list[ mac ];
	}else{
		db.list[ mac ] = {
			mac: mac,
			hostname: hostname || generate_hostname()
		}
		if( !opt.notsave ) await exports.a_save( opt );
		return db.list[ mac ];
	}
}

let lastval;

function generate_hostname(){
	lastval = lastval || Object.keys( db.list || {} )
	.map( mac => {
		return db.list[mac];
	})
	.filter( rec => {
		return rec.hostname.match(/^host\-[0-9]+$/)
	})
	.map( rec => {
		return parseInt((rec.hostname.match(/^host\-([0-9]+)$/))[1]);
	})
	.reduce( ( prev, next ) => {
		return (prev>next)?prev:next;
	}, 0);
	let newhostname;
	do{ 
		lastval++;
		newhostname = 'host-' + ('00000' + (lastval).toString()).substr(-6);
	}while(param_exist( newhostname, 'hostname' ));
	return newhostname;
}

function param_exist( value, param ){
	for (let mac in (db.list || {}) ){
		let rec = (db.list || {})[mac];
		if( rec[param || 'hostname'] == value ) return mac;
	}
	return false;
}

async function createNewDbFileIfNotExist(opt){
	try{
		let stats = await stat( pathtodbjson );
		if(stats.size<=0) throw new Error('no such file');
	}catch(err){
		if( err.message.match(/no such file/) ){
			await writeFile(pathtodbjson, JSON.stringify(eval(newdb), null, 4), 'utf8');
			await chown( pathtodbjson, process.getuid(), process.getgid());
			await chmod( pathtodbjson, '0660');
			info('Новый файл базы данных (' + pathtodbjson + ') создан!', opt);
		}else{
			throw err;
		}
	}
}

function info(mess, {nocolor=false, mute=false}){
	if( !mute ) print( (!nocolor?ESC + '2m':'') + mess + (!nocolor?ESC + '0m':''));
}

exports.a_ls = async function ( {term='', commandmode=false, save=false, fieldname=''} ){
	let list = Object.keys(db.list || {})
			.map( mac => {
				return Object.assign({mac}, db.list[mac]);
			})
			.filter( rec => { 
				return (!term) || 
				( 
					rec.net.match( RegExp(term) ) || 
					rec.ip.match( RegExp(term) ) || 
					rec.hostname.match( RegExp(term) ) ||
					rec.mac.match( RegExp(term) ) ||
					(rec.manufacturer || '').match( RegExp(term) ) ||
					(rec.desc || '').match( RegExp(term) )
				)
			});

	if( commandmode ){
		for( let rec of list ){
			print( 'add ' + rec.mac + ' -m -S' );
			for( let param in rec ){
				if( param!='mac' ) print( 'mac ' + rec.mac + ' "' + param  + '" "' + rec[param] + '"'  + ' -m -S');
			}
		}
		if( save ) print("save");
	}else{
		let fields = [
			{column: 'mac', width: 17, title:'MAC'},
			{column: 'ip', width: 18, title:'IP'},
			{column: 'hostname', width: 30, title:'Имя'},
			{column: 'net', width: 18, title:'Сеть'}
		];
		if( fieldname ){
			fields.push( {column: fieldname, width: 15, title:fieldname} );
			fields.push( {column: 'desc', width: 28, title:'Описание'} );
		}else{
			fields.push( {column: 'manufacturer', width: 45, title:'Производитель'} );
			fields.push( {column: 'desc', width: 28, title:'Описание'} );
		}
		print_table( fields, list );
	}
}


exports.a_scan2db = async function ( net, {nocolor=false, mute=false} ){
	if( !net ) throw new Error('Сеть не может быть пустой');
	if( !mute ) print( (!nocolor?ESC + '2m':'') + 'Процесс может занять нельсколько минут. Подождите...' + (!nocolor?ESC + '0m':''), 0);
	let domen = pkg.get().config.domen;
	let list = await scan( net );
	if( !mute ) print('\r', 0);
	const clist = [];
	for( let i = 0; i < list.length; i++ ){
		if(list[i].match(/Nmap scan report for/)){
			let l = { s1:list[i], s2:'' };
			clist.push( l );
			if(list[i+2] && list[i+2].match(/MAC Address:/)) l.s2 = list[i+2];
		}
	}
	list = clist
		.filter(l=>{
			return l.s1.match(/Nmap scan report for/);
		})
		.map( l=>{
			let a = l.s1.match(/for (.*?) \((.+)\)/);
			if(!a) a = l.s1.match(/for( )([0-9.]+)$/);
			if(a){
				let el = { hostname:a[1], ip:a[2]};
				el.hostname = (el.hostname || '').toString().replace(/\s+/g, '');
				el.hostname = el.hostname || generate_hostname();
				el.hostname = (el.hostname || '').toString().replace(RegExp('.' + domen + '$'), '');
				if(l.s2){
					let a = l.s2.match(/MAC Address: (.+?) \((.+)\)/);
					el.mac = a[1].toLowerCase();
					el.manufacturer = a[2];
				}
				return (el);
			}else{
				//print( l );
				return {};
			}
		});
	let count = 0;
	let count2 = 0;
	if(!db.list) db.list = {};
	list.forEach( e => {
		if(e.mac &&  (e.mac!='ff:ff:ff:ff:ff:ff') ){
			if( (!(e.mac in db.list)) ){
				db.list[ e.mac ] = e;
				db.list[ e.mac ].net = net;
				count++;
			}else{
				db.list[ e.mac ].ip = db.list[ e.mac ].ip || e.ip;
				db.list[ e.mac ].manufacturer =  db.list[ e.mac ].manufacturer || e.manufacturer;
				db.list[ e.mac ].net =  db.list[ e.mac ].net || net;
				count2++;
			}
		}
	});
	if( !mute ) print('\r' + ' '.repeat(120) + '\r', 0);
	info( 'Добавлено новых устройств из сети ' + net + ' - ' + count, {nocolor, mute} );
	info( 'Найдено известных устройств в сети ' + net + ' - ' + count2, {nocolor, mute} );
	return count;
}

exports.a_script_run = async function ( {mute=false, nocolor=false} ){
	let scriptspath = pkg.get().config.scriptspath;
	let files = await readdir(scriptspath);
	files = files.filter( f=>{ return f.match(/\.nw$/)});
	for( let scriptname of files ){
		if( !mute ) print( 'Исполнение скрипта "' +  scriptspath + '/' + scriptname + '" ', 0 );
		let scripttext = await readFile( scriptspath + '/' + scriptname, 'utf8' );
		let scriptarr =  scripttext.split(/\n/).filter( l=>{ return l });
		parser.load( scriptarr, '\t');
		if( !mute ) print(  '(' + await parser.description() + ') ... ', 0 );
		await parser.go( db );
		if( !mute ) print( (!nocolor?ESC + '2m':'') +  '[ ' + (!nocolor?ESC + '32m':'') +  'ok' + (!nocolor?ESC + '0m':'') + (!nocolor?ESC + '2m':'') + ' ]' + (!nocolor?ESC + '0m':'') );
	};
}

exports.a_script_ls = async function ( {nocolor=false} ){
	let scriptspath = pkg.get().config.scriptspath;
	let files = await readdir(scriptspath);
	files = files.filter( f=>{ return f.match(/\.nw$/)});
	for( let scriptname of files ){
		print( 'Скрипт "' +  scriptspath + '/' + scriptname + '" ');
	};
}

async function scan( net ){
	return new Promise(async function(resolve, reject){
		try{
			let path2nmap = (pkg.get().config.sys || {}).nmap || '/usr/bin/nmap'
			try{
				await stat( path2nmap );
			}catch(e){
				if(e.message.match(/no such file/)){
					reject( new Error('NMAP utitlity is not found.') );
				}else{
					reject(e);
				}
			}
			const params = [ path2nmap, '-sP', net ];
			const fs = spawn('sudo', params);
			var err_mess;
			var rows = [];
			fs.stdout.on('data', (r) => {rows.push(r);});
			fs.stderr.on('data', (err) => {err_mess = err.toString();});
			fs.on('close', function(code){
				if(code > 0 || err_mess){
					reject(new Error(err_mess));
				}else{
					resolve(rows.join('').split(/\n/));
				}
			});
		}catch(err){
			reject(err);
		}
	});
}

