/*
*
* Метод инкримирования серийного номера файла зоны.
*
*/

const moment = require('moment');

module.exports = function( serial ){
	var m;
	if(m = serial.match(/(\d\d\d\d\d\d\d\d)(\d\d)/)){
		if(moment().format('YYYYMMDD') == m[1]){
			return m[1].toString() + ('0' + (parseInt(m[2]) + 1)).substr(-2);
		}else{
			return moment().format('YYYYMMDD') + '01';
		};
	}else{
		return serial;
	};
}
