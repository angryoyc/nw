#!/usr/bin/node

'use strict';

const chalk                = require('chalk');
const figlet               = require('figlet');
const { print }            = require('./modules/print_methods');
const { printws }          = require('./modules/print_methods');
const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const stat = promisify(fs.stat);
const spawn = require('child_process').spawn;
const exec = require('./modules/exec');

let core = require('./modules/core');

const ESC               = '\x1b[';
const program = require('./modules/commander_async');
const dns = require('dns');
//const moment = require('moment');

//process.argv.push('script');
//process.argv.push('run');

const pkg = require('./modules/package');
pkg.init(__dirname + '/package.json');

program
	.name(pkg.get().name)
	.version(pkg.get().version)
	.description('\t' + program.getName().toUpperCase() + ' - интерфейс коммандной строки для утилиты подготовки dhcp.conf в соответствии с фактической раздачей адресов');

program
	.command('apply')
	.description('Выгрузка всех конфигураций и перезапуск системных служб (DHCP, iptables, bind, e.t.c.)')
	.action( async function( cmd, argv ){
		let result = await exec(__dirname + '/ctl/apply.sh');
		print( result.join('\n') );
	});


program
	.command('set')
	.description('Установление общих параметров программы')

program
	.command('set')
	.command('db')
	.alias('dbpath')
	.long('<dbpath>')
	.description('Установить путь к файлу базы данных')
	.note('Если вы поменяли путь к базе данных, то  имейте ввиду, что все найденные ранее сведения о ваших сетях, будут утеряны (остануться в старом файле базы данных).')
	.action(async function (cmd, argv) {
		if (cmd.values && cmd.values['dbpath']) argv.dbpath = cmd.values['dbpath'];
		if (argv.dbpath) {
			await core.setDBpath(argv.dbpath, argv);
		} else {
			throw new Error('Bad path: ' + argv.nmappath);
		}
	});

program
	.command('save')
	.description('Сохранить изменения в базе данных')
	.action(async function (cmd, argv) {
		await core.a_save( argv );
	});

program
	.command('scan')
	.long('<net>')
	.description('Сканировать указанную сеть, сохраняя найденные данные в бд')
	.note('Для сохранения найденных узлов используйте команду save')
	.action(async function (cmd, argv) {
		if (cmd.values && cmd.values['net']) argv.net = cmd.values['net'];
		await core.a_scan2db( argv.net, argv );
	});

program
	.command('script')
	.alias('scripts')
	.description('Использование скриптов генерации конфигураций')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.action(async function (cmd, argv) {
		argv.nocolor = !!('-n' in cmd.options);
		if(cmd.commands && Object.keys(cmd.commands).length>0){
			await program.defaultAction(cmd, argv);
		}else{
			await core.a_script_ls( argv );
		}
	});


program
	.command('script')
	.command('run')
	.description('Исполнение скриптов, генерация конфигурационных файлов')
	.option( '-m', '--mute', 'Не выводить сообщений во время выполнения, кроме экстренных', null, 'false' )
	.action(async function (cmd, argv) {
		argv.mute = !!('-m' in cmd.options);
		await core.a_script_run( argv );
	});

program
	.command('script')
	.command('ls')
	.description('Список доступных скриптов')
	.action(async function (cmd, argv) {
		await core.a_script_run( argv );
	});

program
	.command('ls')
	.long('<term>')
	.option( '-c', '--command', 'Вывод списка в виде списка команд', null, 'false' )
	.option( '-s', '--save-command', 'В режиме -с в коце вывода добавляет команду save', null, 'false' )
	.option( '-f', '--field <fieldname>', 'Указывается дополнительное поле для вывода', null, 'false' )
	.description('Вывод данных БД')
	.note('<term> - регулярное выражение, которое будет применяться к каждому из видимых столбцов, таким образом можно использовать символы обозначения начала и конца строки, например для точного нахождения хоста по его ip можно указать "^10.1.2.1$" - будет найдны тольк те записи где этот ip является полным значением одной из колонок, но не будут найдены те записи, где этот ip указан в описании')
	.action(async function (cmd, argv) {
		if (cmd.values && cmd.values['term']) argv.term = cmd.values['term'];
		argv.commandmode = !!('-c' in cmd.options);
		argv.save = !!('-s' in cmd.options);
		if( cmd.options && cmd.options['-f'] && cmd.options['-f'].values && cmd.options['-f'].values['fieldname'] ) argv.fieldname = cmd.options['-f'].values['fieldname'];
		await core.a_ls( argv );
	});

program
	.command('mac')
	.long('<mac> <param> <value>')
	.description('Вывод данных об узле')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-m', '--mute', 'Не выводить сообщений во время выполнения, кроме экстренных', null, 'false' )
	.option( '-S', '--not-save', 'Не сохранять изменения после выполнения команды', null, 'false' )
	.action(async function (cmd, argv) {
		argv.notsave = !!('-S' in cmd.options);
		argv.nocolor = !!('-n' in cmd.options);
		argv.mute = !!('-m' in cmd.options);
		if (cmd.values && cmd.values['mac']) argv.mac = cmd.values['mac'];
		if (cmd.values && cmd.values['param']) argv.param = cmd.values['param'];
		if (cmd.values && cmd.values['value']) argv.value = cmd.values['value'];
		await core.set( argv );
	});

program
	.command('del')
	.long('<mac> <param>')
	.description('Удаление узла по его MAC')
	.option( '-m', '--mute', 'Не выводить сообщений во время выполнения, кроме экстренных', null, 'false' )
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-S', '--not-save', 'Не сохранять изменения после выполнения команды', null, 'false' )
	.action(async function (cmd, argv) {
		argv.mute = !!('-m' in cmd.options);
		argv.nocolor = !!('-n' in cmd.options);
		argv.notsave = !!('-S' in cmd.options);
		if (cmd.values && cmd.values['mac']) argv.mac = cmd.values['mac'];
		if (cmd.values && cmd.values['param']) argv.param = cmd.values['param'];
		await core.del( argv.mac, argv );
	});

program
	.command('add')
	.long('<mac> <hostname>')
	.description('Добавление нового узла по его MAC')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-m', '--mute', 'Не выводить сообщений во время выполнения, кроме экстренных', null, 'false' )
	.option( '-S', '--not-save', 'Не сохранять изменения после выполнения команды', null, 'false' )
	.action(async function (cmd, argv) {
		argv.mute = !!('-m' in cmd.options);
		argv.nocolor = !!('-n' in cmd.options);
		argv.notsave = !!('-S' in cmd.options);
		if (cmd.values && cmd.values['mac']) argv.mac = cmd.values['mac'];
		if (cmd.values && cmd.values['hostname']) argv.hostname = cmd.values['hostname'];
		let host = await core.add( argv.mac, argv.hostname, argv );
		if(!argv.mute) await core.print_host( host );
	});


//                                                                =========== OTHER =============
program
	.command('help')
	.alias('man')
	.long( '[<commandname>] [<commandname2>] [<commandname3>] [<commandname4>] [<commandname5>]' )
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-f', '--full', 'Полный help по всем командам', null, 'false' )
	.option( '-m', '--md', 'Выводить используя разметку md', null, 'false' )
	.description('Вывод короткой справки по командам CLI')
	.action( async function(cmd){
		const color = !('-n' in cmd.options);
		const full = !!('-f' in cmd.options);
		const md = !!('-m' in cmd.options);
		const style = md?2:(color?1:0);
		if( cmd.values && Object.keys(cmd.values).length>0 ){
			const commandnames =  Object.keys(cmd.values).sort().map(c=>{ return cmd.values[c];}).filter(c => {return c;});
			await program.help_of_commands( commandnames, style );
		}else{
			await program.help_main( style );
			if( full ){
				await program.help_full( style );
			}
		}
	});

program
	.command('exit')
	.alias('quit')
	.alias('выход')
	.description('Выход из интерфейса командной строчки')
	.action(async function() {
		program.stop();
		print('Bye Bye..');
		process.exit(0);
	});

program
	.command('do')
	.long('<commandfilename>')
	.description('Выполненить командный файл')
	.note('Команды из файла выполняются одна за одной. Строки, первый значимый (не пробельный) символ которых является знак "#" (решётка) пропускаются.\n\nТаким образом для коментирования текста командного файла можно использовать этот символ (решётка).')
	.action(async function(cmd) {
		//const commandfilename = cmd.values['commandfilename'];
		//await dbconf_lib.do( commandfilename, program, async function( line ){
		//	await program.parse( program.splitCommand( line ) );
		//});
		const commandfilename = cmd.values['commandfilename'];
		await program.doit( commandfilename, async function( line ){
			await program.parse( program.splitCommand( line ) );
		});
	});


//                                   ------------- START --------------

program.start(
	async ()=>{
		//- start
		await core.a_init( pkg );
	},
	async ()=>{
		//- stop
	},
	async (err)=>{
		//- error
		print( ESC + '31m' +  'ERR: ' + ESC + '0m' +  err.message );
	},
	async ()=>{
		//- title
		//title
		print('');
		print( chalk.green(figlet.textSync(program.getName().toUpperCase(), { horizontalLayout: 'full' })) );
		print( '\n' + 'Version: ' + program.getVersion() );
		print( '\n' + 'Используй команду help, если не знаешь, что делать дальше \n' );
	}
);
